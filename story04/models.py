from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
    day = models.CharField(max_length=9)
    date = models.DateField()
    time = models.TimeField()
    activity_name = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
