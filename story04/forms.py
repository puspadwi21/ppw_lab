from django import forms

class JadwalPribadiForms(forms.Form):
	error_messages = {
		'required': 'Missing input!'
	}

	day_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'What Day Does Your Activity Take Place?'
	}

	date_attrs = {
		'type': 'date',
        'class': 'form-control',
        'placeholder':'When Is the Date of Your Activity?'
	}

	time_attrs = {
		'type': 'time',
        'class': 'form-control',
        'placeholder':'What Is the Specicic Time?'
	}

	activityName_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'Name Your Activity!'	
	}

	place_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'Where Will This Activity Be?'
	}

	category_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'Under What Category This Activity Is?'
	}

	day = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=day_attrs))
	date = forms.DateField(label='', required=True, widget=forms.DateInput(attrs=date_attrs))
	time = forms.TimeField(label='', required=True, widget=forms.TimeInput(attrs=time_attrs))
	activity_name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=activityName_attrs))
	place = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=place_attrs))
	category = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=category_attrs))