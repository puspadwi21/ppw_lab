from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name = 'lab3html'),
    path('about', views.about, name = 'about'),
    path('myrecords', views.myrecords, name = 'myrecords'),
    path('lifeinphotos', views.lifeinphotos, name = 'lifeinphotos'),
    path('blog', views.blog, name = 'blog'),
    path('visualdesign', views.visualdesign, name = 'visualdesign'),
    path('guestform', views.guestform, name = 'guestform'),
    path('setschedule', views.setschedule, name = 'setschedule'),
    path('saveschedule', views.saveschedule, name = 'saveschedule'),
    path('myschedule', views.myschedule, name = 'myschedule'),
    path('deleteschedule', views.deleteschedule, name = 'deleteschedule'),
]

##
