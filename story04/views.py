from django.shortcuts import render
from .forms import JadwalPribadiForms
from .models import JadwalPribadi
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def home(request):
    return render(request, 'lab3html.html')

def about(request):
    return render(request, 'about.html')

def myrecords(request):
    return render(request, 'myrecords.html')

def lifeinphotos(request):
    return render(request, 'lifeinphotos.html')

def blog(request):
    return render(request, 'blog.html')

def visualdesign(request):
    return render(request, 'visdes.html')

def guestform(request):
    return render(request, 'formtamu.html')

def setschedule(request):
	showform = JadwalPribadiForms()
	response['showform'] = showform
	return render(request, 'setschedule.html', response)

def saveschedule(request):
	form = JadwalPribadiForms(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['time'] = request.POST['time']
		response['activity_name'] = request.POST['activity_name']
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		schedule = JadwalPribadi(day=response['day'], date=response['date'], time=response['time'], activity_name=response['activity_name'], place=response['place'], category=response['category'])
		schedule.save()
		return HttpResponseRedirect('/myschedule')
	else:
		return HttpResponseRedirect('/setschedule')

def myschedule(request):
	schedule = JadwalPribadi.objects.all()
	response['schedule'] = schedule
	return render(request, 'myschedule.html', response)

def deleteschedule(request):
    delete = JadwalPribadi.objects.all()
    delete.delete()
    return HttpResponseRedirect('/myschedule')
